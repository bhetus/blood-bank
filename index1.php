<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ro-RO">
<head profile="http://gmpg.org/xfn/11">
<title>ERROR 404 Page</title>
<meta charset="utf-8"/>

<meta http-equiv="X-UA-Compatible" content="IE=8">
<link href='http://fonts.googleapis.com/css?family=Istok+Web|Chivo' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media="all" href="../home/css/error_style.css" />
<link rel="stylesheet" type="text/css" media="all" href="../home/css/backgrounds.css" />
<!--<link rel="stylesheet" type="text/css" media="all" href="themes/blue/css/style.css" />-->
<link rel="stylesheet" type="text/css" media="all" href="themes/green/css/style.css" />
<!--<link rel="stylesheet" type="text/css" media="all" href="themes/gray/css/style.css" />-->


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript" src="../home/js/jquery-global.js"></script>

<!--[if IE]>
<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>


<div class="wrapper">

	<div class="mainWrapper">
        <div class="leftHolder">
        
            <div class="errorNumber">404</div> 
        </div>
        <div class="rightHolder">
            <div class="message"><p>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p></div>
            <div class="robotik"><img src="../home/error_images/robotik.png" alt="Oooops....we can’t find that page." title="Oooops....we can’t find that page." id="robot"></div>
            <div class="tryToMessage">
                Try to:
                <ul>
                    <li>Use the search form</li>
                    <li>Visit the <a href="" title="Robotik Sitemap">Sitemap</a></li>
                    <li>Go <a href="javascript:history.go(-1)" title="Back">back</a></li>
                </ul>
            </div>
            <!-- Search Form -->
            <div class="search">
            <span class="errorSearch">Please fill the search field</span>
            <form action="" method="post">
              <div class="searchInput">
                <input type="text" name="search_term" value="Search" />
              </div>
              <div class="searchButton">
                <input type="submit" name="submit" value="Go" />
              </div>
            
            </form>
            </div>
            <!-- end .search -->
          </div>
      


        <footer>
        <p class="copy">&copy 2013 Makura Creations . All rights reserved.</p>
<!--        <menu>
            <li><a href="<?php echo site_url();?>/home/index" title="Home">Home</a></li>
            <li><a href="<?php echo site_url();?>/home/page/<?php echo $id=11;?>" title="About Us">About us</a></li>
           
            <li class="last"><a href="<?php echo site_url();?>/home/page/<?php echo $id=14;?>" title="Contact">Contact</a></li>
        </menu>-->
        </footer>
        <!-- end footer -->

	</div>

</div>
<!-- end .wrapper -->


</body>
</html>
