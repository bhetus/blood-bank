<?php

class Donation extends MX_Controller {

    function __construct() {
        parent::__construct();
        ob_start();
        $this->load->model('blood/blood_model');
        $this->load->model('donation/donation_model');
        $this->load->model('user/ion_auth_model');
        if (!$this->input->is_ajax_request()) {
            modules::run('users/auth/check_login');
        }
    }

    function index() { 
        $this->getAll();
    }
 
  
    public function add() {
        if ($_POST) {
            $this->donation_model->manage();
            $this->getAll();
        } else {
            $data['title'] = "Add Donation";
	    $data['blood'] = $this->blood_model->getAll();
	    $data['users'] = $this->ion_auth_model->getAll();
		//print_r($data['parent']);die();
            $this->load->view('base/header', $data);
            $this->load->view('donation_form');
            $this->load->view('base/footer');
        }
    }

    /*    Function to validate the post of the form     */

    function getAll() {

        $data['title'] = "View existing Donations";        
        $data['all'] = $this->donation_model->getAllJoined();
        $this->load->view('base/header', $data);
        $this->load->view('donation/donation_table');
        $this->load->view('base/table_footer');
    }

    /*
     * Function to update the destination 
     * id:destinaion identity  as argument 
     */

    function edit($id = '') {
//        $id = $this->uri->segment(3);
        if ($_POST) {
            $data = $this->donation_model->getSingle($id);      
//print_r($img);die();
            $this->donation_model->manage($id);
            redirect('donation');
        } else {
             $data['blood'] = $this->blood_model->getAll();
	    $data['users'] = $this->ion_auth_model->getAll();
            $data['single'] = $this->donation_model->getSingle($id);
           
           
            $data['title'] = "Edit Donation";
            $this->load->view('base/header', $data);
            $this->load->view('donation_form');
            $this->load->view('base/footer');
        }
    }

    /*
     * Function to Delete donation
     * id:donation identity  as argument 
     */

    function delete($id) {
        $this->donation_model->delete($id);
        redirect('index.php/donation');
    }

}

?>
