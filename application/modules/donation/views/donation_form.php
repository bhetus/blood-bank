<ol class="breadcrumb bc-3">
    <li>
        <a href="index.html"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url(); ?>donation">Donation</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>donation" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Donations
</a>
<h2>Add Donation</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">


            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Blood Type : </label>

                    <select class="form-control" name="blood_id"> 
                        <!--<option value="0">none</option>-->
                        <?php foreach ($blood as $b): ?>

                            <option value="<?php echo $b->id; ?>" <?php
                            if (!empty($single)) {
                                if ($b->id == $single->blood_id) {
                                    echo 'selected="selected"';
                                }
                            }
                            ?>><?php echo $b->blood_group; ?></option>
                                <?php endforeach; ?>
                    </select>
                </div>
            </div>	    
            <div class="clearfix"></div>

            <div class="col-md-12   ">
                <div class="form-group">
                    <label class="control-label">Donation By : </label>

                    <select class="form-control" name="user_id"> 
                        <?php foreach ($users as $u): ?>

                            <option value="<?php echo $u->id; ?>" <?php
                            if (!empty($single)) {
                                if ($u->id == $single->user_id) {
                                    echo 'selected="selected"';
                                }
                            }
                            ?>><?php echo $u->first_name; ?></option>
                                <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Donation Date </label>
                    <input class="form-control datepicker" name="date" id="reservation_date" placeholder="Donated Date" data-validate="required,date" value="<?php
                    if (!empty($single)) {
                        echo $single->date;
                    }
                    ?>"/>

                </div>
            </div> 
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Amount</label>

                    <input type="text" class="form-control" name="amount" data-validate="required|number" data-message-required="This field is required" value="<?php
                    if (!empty($single)) {
                        echo $single->donation_amount;
                    }
                    ?>"/>
                </div>
            </div> 




            <div class="col-md-11">
                <div class="form-group">
                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                    <button type="reset" class="btn">Reset</button>
                </div>
        </form>

    </div>
</div>

</div><!-- Footer -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>