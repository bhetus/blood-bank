<?php

class Donation_model extends CI_Model {

    private $table = "donation";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($id = '') {
        $blood_id = $this->input->post('blood_id');
        $amount = $this->input->post('amount');
        $data = array(
            'blood_id' => $blood_id,
            'user_id' => $this->input->post('user_id'),
            'donation_amount' => $amount,
            'date' => $this->input->post('date'),
        );
//        data for blood_type


        if ($id == '') {
//       first insert the data in donation table
            $this->db->insert($this->table, $data);
//            secondly update the blood_type table to update the blood amount in blood_type table 
            $newBloodAmount = $this->getTotalBloodOfType($blood_id);
            $bdata['amount'] = $newBloodAmount;
            $this->db->where('id', $blood_id);
            $this->db->update('blood_type', $bdata);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);

            $newBloodAmount = $this->getTotalBloodOfType($blood_id);
            $bdata['amount'] = $newBloodAmount;
          
//            update amount  
            $this->db->where('id', $blood_id);
            $this->db->update('blood_type', $bdata);
            $this->session->set_flashdata('message', "Information updated successfully");
        }
    }

    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }

    function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
        $this->session->set_flashdata('message', "Information Deleted successfully");
    }

    /* front end functions start here */

    function getBloodById($bid) {
        $query = $this->db->query("Select * from blood_type where id='$bid'");
        return $query->row($bid);
    }

    function getAllonHome() {
        $query = $this->db->query("select * from mk_page where publish=1 and featured=1");
        return $query->result();
    }

    function getAllJoined() {
        $query = $this->db->query("select d.*,b.id as bid,b.amount,b.blood_group,u.first_name from donation d INNER JOIN blood_type b on b.id=d.blood_id INNER JOIN users u on u.id=d.user_id ");
        return $query->result();
    }

    function getTotalBloodOfType($bid) {
        $query = $this->db->query("select SUM(donation_amount) as bloodamount from donation where blood_id='$bid'");
        $data = $query->row();
        return $data->bloodamount;
    }

}

?>
