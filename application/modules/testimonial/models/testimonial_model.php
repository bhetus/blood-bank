<?php

class Testimonial_model extends CI_Model {
    
    private $table = "mk_testimonial";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($img='',$id='') {
	$name=$this->input->post('person_name');
	$slug = url_title($name, 'dash', true);
      
        $data = array(
            'person_name' => $this->input->post('person_name'),
            'slug' => $slug,
	    'country' => $this->input->post('country'),
	    'image'=>$img,
            'description' => $this->input->post('description'),      
	    'publish' => $this->input->post('publish'),  
        );

        if ($id == '') {
            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {           
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message',"Information updated successfully");
        }
    }

   
    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

  
    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }
    
    function getParent(){
	$data = $this->db->query("SELECT * FROM $this->table WHERE parent_id=0");
	return $data->result();
    }

    function getChild(){
	$data = $this->db->query("SELECT * FROM $this->table WHERE parent_id!=0");
	return $data->result();
    }
    
    function delete($id){
        $this->db->delete($this->table,array('id'=>$id));
        $this->session->set_flashdata('message',"Information Deleted successfully");        
    }



    function getPublishedTestimonial(){
	$data=$this->db->query("SELECT * FROM $this->table WHERE publish='1' ");
	return $data->result();
    }

   
}

?>
