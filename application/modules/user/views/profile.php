
<div class="profile-env">
	
	<header class="row">
		
		<div class="col-sm-2">
			
			<a href="#" class="profile-picture">
				<img src="<?php echo base_url();?>uploads/user_images/thumb/<?php echo $user->image;?>" class="img-responsive img-circle" />
			</a>
			
		</div>
		
		<div class="col-sm-7">
			
			<ul class="profile-info-sections">
				<li>
					<div class="profile-name">
						<strong>
							<a href="#"><?php echo $user->first_name . ' ' . $user->last_name;?></a>
							<a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
							<!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
						<span><a href="#">Member The Last Resort</a></span>
					</div>
				</li>
				
				
			</ul>
			
		</div>
		
		<div class="col-sm-3">
			
			<div class="profile-buttons">
				<a href="<?php echo base_url();?>user/edit/<?php echo $user->id;?>" class="btn btn-default">
					<i class="entypo-user"></i>
					Update Profile
				</a>
				
				
			</div>
		</div>
		
	</header>
	
	<section class="profile-info-tabs">
		
		<div class="row">
			
			<div class="col-sm-offset-2 col-sm-10">
				
				<ul class="user-details">
					<li>
						<a href="#">
							<i class="entypo-mail"></i>
							<?php echo $user->email;?>
						</a>
					</li>
					
					<li>
						<a href="#">
							<i class="entypo-phone"></i>
							 <span><?php echo $user->phone;?></span>
						</a>
					</li>
					
					<li>
						<a href="#">
							<i class="entypo-calendar"></i>
                                                        Registered at : <span><?php echo unix_to_human($user->created_on);?></span>
						</a>
					</li>
                                        
                                        <li>
						<a href="#">
							<i class="entypo-login"></i>
                                                        Last Login at : <span><?php echo unix_to_human($user->last_login);?></span>
						</a>
					</li>
				</ul>
				
				
				
				
			</div>
			
		</div>
		
	</section>
	
	
	
						