<?php

class Image extends MX_Controller {

    function __construct() {
        parent::__construct();
	$this->load->library('image_moo');
	//$this->load->library('user/ion_auth');
        ob_start();
        $this->load->model('gallery/gallery_model');
        $this->load->model('gallery/image_model');
	$this->load->model('category/category_model');
        if (!$this->input->is_ajax_request()) {
            modules::run('users/auth/check_login');
        }
    }

    function index() {
        $this->getAll();
	
    }

   function do_upload() {
        $name_array = array();
        $count = count($_FILES['userfile']['size']); 
        foreach ($_FILES as $key => $value)
            for ($s = 0; $s <= $count - 1; $s++) {
                $_FILES['userfile']['name'] = $value['name'][$s];
                $_FILES['userfile']['type'] = $value['type'][$s];
                $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                $_FILES['userfile']['error'] = $value['error'][$s];
                $_FILES['userfile']['size'] = $value['size'][$s];
                $config['upload_path'] = 'uploads/gallery_images/original';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '4000';
                $config['max_width'] = '2000';
                $config['max_height'] = '2000';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload("userfile")) {
                    $data = $this->upload->data();
                    /* PATH */
                    $source = "uploads/gallery_images/original/" . $data['file_name'];
                    $destination_medium = "uploads/gallery_images/resized/";
                    $destination_thumbs = "uploads/gallery_images/thumb/";
                    $size_medium_width = 360;
                    $size_medium_height = 243;
                    $size_thumb_width = 165;
                    $size_thumb_height = 165;
                    $this->image_moo
                            ->load($source)
                            /* RESIZING IMAGE TO BE MEDIUM SIZE */
                            ->resize_crop($size_medium_width, $size_medium_height)
                            ->save($destination_medium . $data['file_name'])
                            /* CROPPING IMAGE TO BE THUMBNAIL SIZE */
                            ->resize_crop($size_thumb_width, $size_thumb_height)
                            ->save($destination_thumbs . $data['file_name']);
                    if ($this->image_moo->errors)
                        print $this->image_moo->display_errors();
                    else {

                        $name_array[] = $data['file_name'];
                    }
                }
            }

          return   $name_array;
    }

    function add() {
        if ($_POST) {
	    $img = $this->do_upload();
            $this->image_model->manage($img);
            $this->getAll();
        } else {
            $data['title'] = "Add Image";
			$data['category']=$this->gallery_model->getAll();			
            $this->load->view('base/header', $data);
            $this->load->view('gallery/image_form');
            $this->load->view('base/footer');
        }
    }

    /*    Function to validate the post of the form     */

    function getAll() {
        $data['title'] = "View existing Pages";        
        $data['all'] = $this->image_model->getAll();
        $this->load->view('base/header', $data);
        $this->load->view('gallery/image_table');
        $this->load->view('base/table_footer');
    }

    /*
     * Function to update the destination 
     * id:destinaion identity  as argument 
     */

    function edit($id = '') {
        if ($_POST) {
            $data = $this->image_model->getSingle($id);
	    $old = $data->image;

      if($_FILES["userfile"]["name"][0] != null){ 
            $img = $this->do_upload();
        } else {
            $img = $old;
        }

            $this->image_model->manage($img,$id);
            redirect('gallery/image');
        } else {
            $data['single'] = $this->image_model->getSingle($id);
           
            if (empty($data['single'])) {
                show_404();
            }
            $data['title'] = "Edit Image";
	    $data['category']=$this->gallery_model->getAll();
            $this->load->view('base/header', $data);
            $this->load->view('gallery/image_form');
            $this->load->view('base/footer');
        }
    }

    /*
     * Function to Delete image 
     * id:image identity  as argument 
     */

    function delete($id) {
        $this->image_model->delete($id);
        redirect('gallery/image');
    }



}

?>
