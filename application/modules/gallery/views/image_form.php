<ol class="breadcrumb bc-3">
    <li>
        <a href="index.html"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url(); ?>gallery/image">Image</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>gallery/image" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Image
</a>
<h2>Add Image</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">

	    <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Image Name</label>

                <input type="text" class="form-control" name="name" data-validate="required" data-message-required="This field is required" placeholder="Image name here" value="<?php if(!empty($single)){ echo $single->title;}?>"/>
            </div>
	    </div> 
		
		<div class="col-md-6">
	                <div class="form-group">
                <label class="control-label">Select Gallery : </label>

                <select class="form-control" name="gallery_id"> 
			<option value="0">none</option>
			<?php foreach($category as $c): ?>
                 <option value="<?php echo $c->id;?>" <?php if(!empty($single)){ if($single->cat_id == $c->id){echo 'selected="selected"';}}?>> <?php echo $c->title;?> </option>
			<?php endforeach;?>
			</select>
            </div>
	    </div>	    

			<div class="col-md-6">
	<div class="form-group">
                <label class="control-label">Images</label><br>

<input type="file" name="userfile[]" class="form-control file2 inline btn btn-primary" multiple=""><font color="red" data-label="<i class='glyphicon glyphicon-circle-arrow-up'></i> &nbsp;Browse Files"></font>

            </div>
            </div>
			

	<div class="col-md-11">
      <div class="form-group">
                <button type="submit" class="btn btn-success" name="submit">Submit</button>
                <button type="reset" class="btn">Reset</button>
            </div>
        </form>

    </div>
	</div>

</div><!-- Footer -->
