<?php

class Image_model extends CI_Model {

    private $table = "mk_image";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($img = '', $id = '') {


        $cat_id = $this->input->post('gallery_id');
        $title = $this->input->post('name');

        if ($id == '') {

            foreach ($img as $i) {
                $this->db->query("INSERT INTO $this->table (title,image,cat_id) values('$title','$i','$cat_id')");
            }

            $this->session->set_flashdata('message', "Insertion successfull");
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message', "Information updated successfully");
        }
    }

    function getAll() {
        $query = $this->db->query("SELECT i.*,c.title as cname from mk_image i INNER JOIN mk_gallery_category c on c.id = i.cat_id ORDER BY i.id Desc");
        return $query->result();
    }

    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }

    function getAccToGallery($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE cat_id='$id'");
        return $data->result();
    }

    function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
        $this->session->set_flashdata('message', "Information Deleted successfully");
    }

    function search($search) {
        $data = $this->db->query("select * from $this->table where name like '$search%'");
        return $data->result();
    }

    function getProductByCategorySlug($slug) {
        $this->db->select('mk_product.*,mk_category.cat_name');
        $this->db->join('mk_category', 'mk_product.cat_id=mk_category.id', 'INNER');
        $data = $this->db->get_where('mk_product', array('mk_category.slug' => $slug));
        return $data->result();
    }

    function getProductBySlug($slug) {
        $this->db->select('mk_product.*,mk_category.cat_name');
        $this->db->join('mk_category', 'mk_product.cat_id=mk_category.id', 'INNER');
        $data = $this->db->get_where('mk_product', array('mk_product.slug' => $slug));
        return $data->row($slug);
    }

    function getFeaturedProduct() {
        $data = $this->db->query("SELECT * FROM $this->table WHERE featured='1'ORDER BY id desc LIMIT 8");
        return $data->result();
    }

}

?>
