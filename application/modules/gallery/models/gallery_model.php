<?php

class Gallery_model extends CI_Model {

    private $table = "mk_gallery_category";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    function manage($img = '', $id = '') {
        $name = $this->input->post('name');
        $title = $this->input->post('name');
        $slug = url_title($title, 'dash', true);

        $data = array(
            'title' => $this->input->post('name'),
            'image' => $img,
            'description' => $this->input->post('description'),
            'slug' => $slug,
            'publish' => $this->input->post('publish'),
            'featured' => $this->input->post('featured'),
        );

        if ($id == '') {

            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message', "Information updated successfully");
        }
    }

    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }

    function slugtoid($slug) {

        $data = $this->db->query("SELECT * FROM $this->table WHERE slug='$slug'");

        //print_r($data); die();
        $cat_id = $data->row($slug);
        if (!empty($cat_id)) {
            return $cat_id->id;
        }
    }

    function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
        $this->session->set_flashdata('message', "Information Deleted successfully");
    }

    function search($search) {
        $data = $this->db->query("select * from $this->table where name like '$search%'");
        return $data->result();
    }

    function getProductByCategorySlug($slug) {
        $this->db->select('mk_product.*,mk_category.cat_name');
        $this->db->join('mk_category', 'mk_product.cat_id=mk_category.id', 'INNER');
        $data = $this->db->get_where('mk_product', array('mk_category.slug' => $slug));
        return $data->result();
    }

    function getProductBySlug($slug) {
        $this->db->select('mk_product.*,mk_category.cat_name');
        $this->db->join('mk_category', 'mk_product.cat_id=mk_category.id', 'INNER');
        $data = $this->db->get_where('mk_product', array('mk_product.slug' => $slug));
        return $data->row($slug);
    }

    function getFeaturedProduct() {
        $data = $this->db->query("SELECT * FROM $this->table WHERE featured='1'ORDER BY id desc LIMIT 8");
        return $data->result();
    }

}

?>
