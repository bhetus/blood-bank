  <section id="donateBanner">
    <div class="mainWrapper">
        <div class="page">
            <div class="container">
                <div class="breadcrumbs">
                    <ul>
                <li><a href="">3 Columns Grid</a></li>
                <li><a href="">3 Columns Grid</a></li>
                <li><a href="">3 Columns Grid</a></li>
                </ul>
                <h1>you have what they need</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="mainWrapper">
    <div class="page">
        <div class="container">
<!-- left column -->
            <div class="articleContent">
    <div class="colWrapper">
<!-- left column inside article -->
        <div class="fixedNav">
            <a href="">become a member</a>
        </div>
        
<!-- searchform -->
        <div class="centralContentWrapper innerWrapper">
          <div class="centralContainer">
           <h2>Search Blood</h2>
           <div class="line"></div>
            <form class="searchBloodForm" method="post" action="<?php echo site_url();?>/home/search">
               <div class="fieldWrapper">
                <label class="title">Bloodgroup</label>
                <select class="optionClass" name="blood_type">
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>
                    <option value="AB+">AB+</option>
                    <option value="AB-">AB-</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>
                </select>
                <div class="clear"></div>
                </div>
                <div class="fieldWrapper">
                <label class="title">City</label>
                <select class="optionClass" name="location">
                    <option value="">select any one</option>
                    <option value="Kathmandu">kathmandu</option>
                    <option value="Lalitpur">Lalitpur</option>
                    <option value="Bhaktapur">Bhaktapur</option>
                </select>
                <div class="clear"></div>
                </div>
                <!-- <input class="login-btn" type="submit" value="Search"> -->
              <?php  
              echo form_open('user/search');
              echo form_submit('btnsearch', 'Search', "class='login-btn'"); 
              echo form_close();
              ?>
            <div class="clear"></div>
            </form>
            </div>
        </div>
<!-- beginning of article body -->
         <div class="centralContentWrapper innerWrapper">
          <div class="centralContainer">
          <?php if(!empty($result)) { ?>
    <section id="panelRow">
        
<?php foreach($result as $r):?>
        <div class="panelRowContainerWrapper">
        <div class="panelRowContainer">
           <div class="imgWrapper">
            <img src="assets/images/second.png" alt="image">
            </div>
            <div class="articleBody">
            <h3><?php echo $r->blood_type;?></h3>
            <p>
                Donor Name : <?php echo $r->name;?> <br>
                Location : <?php echo $r->location;?>
            </p>
             <a href="">learn more</a>
            </div>
            <div class="clear"></div>
<?php endforeach ; ?>  

            
        </div>
        </div>

    </section>
<?php }?>            
          </div>
        </div>

                        
<!-- end of left content inside article body -->
                <div class="clear"></div> 
            </div>
         </div>   

<!-- right column -->
        <aside class="sideBar">
    <div class="innerWrapper">
    <div class="promo light-red">
        <img src="<?php echo base_url(); ?>assets/images/img3.jpg" alt=""></img>
        <p>Donate blood now.People cant live without it.</p>
        <a class="callout" href="">Donate now</a>
    </div>
    </div>
       <div class="innerWrapper">
    <div class="promo blue">
         <img src="<?php echo base_url(); ?>assets/images/img3.jpg" alt=""></img>
        <p>One pint of blood can save life of three people.</p>
        <a class="callout" href="">More facts</a>
    </div>
    </div>
</aside>
     <div class="clear"></div>   
    </div>
</div>
</div>