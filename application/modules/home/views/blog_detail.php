

<body>
    <!-- header ends -->
    
    <!-- aritcle section -->
    <div class="mainWrapper">
        <div class="page">
            <div class="container">
                <!-- left column -->
                <!--starting of post-->
                
                    <div class="articleContent">
                        <div class="articleHeader">
                            <div class="dateMonth">
                                <div class="date"><?php $date=explode('-',$post->date); echo $date[0]?></div>
                                <div class="month"><?php echo $date[1];?></div>
                            </div>
                            <div class="postTitle">
                                <h2><?php echo $post->name;?></h2>
                                <div class="postBy">written by <i>Admin</i> 
                                </div>
                            </div>
<!--                            <div>
                                <img src="<?php echo base_url(); ?>/assets/home/images/img7.jpg" class="postLogo">
                            </div>-->
                            <div class="clear"></div>
                        </div>
                        <div class="article1Pic">
                            <img src="<?php echo base_url(); ?>/uploads/page_images/resized/<?php echo $post->image;?>">
                        </div>
                        <div class="articleTextArea">
                           <?php echo $post->description;?>
                           
                            </p>
                        </div>
                        <div class="postMeta">
<!--                            <div class="postType">
                                <div class="iconContainer"> <i class="fa fa-thumb-tack"></i> 
                                </div>
                            </div>-->
                            <span class="post-comments"></span> 
                                <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'bloodbanknepal'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    
                        </div>
                    </div>
                <!--  side bar  -->
                <div class="sideBar">
                    <div class="tabWidget">
                        <ul class="tabs">
                            <li>
                                <input type="radio" checked name="tabs" id="tab1">
                                <label for="tab1">Recent Post</label>

                                <div id="tab-content1" class="tab-content">
                                    
                                    <?php foreach($recent as $r):?>
                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/uploads/page_images/thumb/<?php echo $r->image;?>">
                                            </a>
                                        </div>
                                        <div class="info"> <span class="widgettitle"> <a href="<?php echo base_url();?>index.php/home/detail/<?php echo $r->slug;?>"title="Blog Post with Youtube Video"><?php echo $r->name;?></a> </span>  <span class="meta">
                                                <div datetime="2014-02-28"><i class="fa fa-clock-o"></i><?php echo $r->date;?></div>
                                            </span> 
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    
                                     <?php endforeach;?>
                                   
                                </div>
                            </li>


                          
                            <div class="clear"></div>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>                        <!-- closing of sidebar -->
                <div class="clear"></div> 
            </div>                   <!-- closing of container -->
        </div>                       <!-- closing of page -->
    </div>   <!-- closing of mainwrapper -->
    <section id="quote">
        <div class="mainWrapper">
            <div class="page">
                <div class="container">
                    <div class="quoteContent">
                        <div class="quoteSign fa fa-quote-right"></div>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority look even slightly believable.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--   footer section -->

    <!--   footer ends-->

    <script src="<?php echo base_url(); ?>assets/home/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/home/js/slider.js"></script>
    <script src="<?php echo base_url(); ?>assets/home/js/jquery.cycle.all.js"></script>
      <!--  <script>
           $(document).ready(function () {
               $(".panel_button").click(
                   function () {
                       $("#panel").animate({
                           height: "400px"
                       });
                       $("panel_button").toggle();
                   });
               $("#hide_button").click(function () {
                   $("#panel").animate({
                       height: "0px"
                   });
               });
           });
       </script> -->
    <script>
        $("#site-header-wrapper .icon").click(function() {
            $('.nav-menu').slideToggle();
        });

    </script>

</body>
</html>
