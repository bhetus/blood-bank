<script src="<?php echo base_url(); ?>assets/home/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/home/js/slider.js"></script>
<script src="<?php echo base_url(); ?>assets/home/js/jquery.cycle.all.js"></script>
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<script type="text/javascript">
    var baseurl = "<?php echo base_url(); ?>";
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/home/css/style.css">
<div class="overlay" id="overlay"></div>
<div id="signin-wrapper">
    <div class="page">
        <div class="container">
            <button class="signin-btn">SignIn</button>
            <div class="clear"></div>
            <div class="signin-content">
                <div class="back"><img src="<?php echo base_url(); ?>assets/home/images/back.png">
                </div>
                <div id="error" style="display:none;padding:5px;"></div>
                <form method="post" action="" id="loginForm">
                    <?php
                    // $attributes = array('class' => 'signin-form'); 
                    // echo form_open('site/login', $attributes);

                    echo form_input('username', '', 'placeholder="Username"');
                    echo form_password('password', '', 'placeholder="Password"');
                    if (isset($message_error) && $message_error) {
                        echo '<div class="alert alert-error">';
                        echo '<a class="close" data-dismiss="alert">×</a>';
                        echo '<strong>Oh snap!</strong> Change a few things up and try submitting again.';
                        echo '</div>';
                    }
                    echo "<br />";
                    echo anchor('home/register', 'Signup!');
                    echo "<br />";
                    echo "<br />";
                    echo form_submit('submit', 'Login', 'class="login-btn sigin"');
                    ?>
                </form>

                <div class="lock"><img src="<?php echo base_url(); ?>assets/home/images/lockBig.png">
                </div>    
            </div>
        </div>
    </div>
</div>
<header id="site-header-wrapper">
    <div class="page">
        <div class="container">
            <div class="site-header-branding">
                <img src="<?php echo base_url(); ?>assets/home/images/logo1.jpg">
            </div>
            <nav class="nav-menu">
                <ul>
                    <li class="mainNav"><a href="<?php echo base_url(); ?>">HOME</a>
                        <div class="over"></div>
                        <ul class="subMenu">
                            <li class=""><a href="">3 Columns Grid</a></li>
                            <li class=""><a href="">3 Columns Grid</a></li>
                            <li class=""><a href="">3 Columns Grid</a></li>
                            <li class=""><a href="">3 Columns Grid</a></li>
                            <li class=""><a href="">3 Columns Grid</a></li>
                            <li class=""><a href="">3 Columns Grid</a></li>
                        </ul>
                    </li>
                    <li class="mainNav"><a href="">FEATURES</a></li>
                    <li  class="mainNav"><a href="">PAGES</a></li>
                    <li  class="mainNav"><a href="<?php echo base_url(); ?>home/blog">Blog</a></li>
                    <li class="mainNav"><a href="">contact</a></li>
                    <div class="clear"></div>
                </ul>
            </nav>
            <div class="clear"></div>
        </div>
    </div>
    <script>
        $(".sigin").click(function() {
            var dataString = $("#loginForm").serialize();
            var baseurl = "<?php echo base_url(); ?>user";
            $.ajax({
                'url': 'user/login',
                'method': 'POST',
                'data': dataString,
                dataType: 'json',
                'success': function(response) {
                    var login_status = response.login_status;
                     
                    if (login_status == 'success')
                    {
                        // Redirect to login page
                        setTimeout(function()
                        {
                            var redirect_url = baseurl;

                            window.location.href = redirect_url;
                        }, 400);
                    }

                },
                'error':function(){
                    $("#error").show().html("Invalid Username or Password");
                }
            });
            return false;
        });
    </script>
</header>
