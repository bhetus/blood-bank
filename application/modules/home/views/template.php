<!DOCTYPE html>
<html>
<head>
	<title>Bloodbank</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
</head>
<body>
<?php $this->load->view('includes/main_header'); ?>

<?php $this->load->view($main_content); ?>

<?php $this->load->view('includes/footer'); ?>
<!--   footer ends-->
</body>


</html>


