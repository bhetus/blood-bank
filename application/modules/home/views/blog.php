

<body>
    <!-- header ends -->
    <section id = "slider"> 
        <div class="slider">
            <ul id="slider1">
                <?php foreach ($slider as $s): ?>
                    <li>
                        <img src="<?php echo base_url(); ?>/uploads/slider_images/resized/<?php echo $s->image; ?>" alt="jQuery Image slider" />
                    </li>
                <?php endforeach; ?>

            </ul>
        </div>
        <div id="slider-nav">
            <img id="prev" src="<?php echo base_url(); ?>/assets/home/images/prev-arrow.png">
            <img id="next" src="<?php echo base_url(); ?>/assets/home/images/next-arrow.png">
        </div>
    </section>

    <!-- aritcle section -->
    <div class="mainWrapper">
        <div class="page">
            <div class="container">
                <!-- left column -->
                <!--starting of post-->
                <?php foreach ($posts as $p): ?>
                    <div class="articleContent">
                        <div class="articleHeader">
                            <div class="dateMonth">
                                <div class="date"><?php $date = explode('-', $p->date);
                echo $date[0] ?></div>
                                <div class="month"><?php echo $date[1]; ?></div>
                            </div>
                            <div class="postTitle">
                                <h2><?php echo $p->name; ?></h2>
                                <div class="postBy">written by <i>Admin</i> 
                                </div>
                            </div>
                            <!--                            <div>
                                                            <img src="<?php echo base_url(); ?>/assets/home/images/img7.jpg" class="postLogo">
                                                        </div>-->
                            <div class="clear"></div>
                        </div>
                        <div class="article1Pic">
                            <img src="<?php echo base_url(); ?>/uploads/page_images/resized/<?php echo $p->image; ?>">
                        </div>
                        <div class="articleTextArea">
    <?php echo substr($p->description, 0, 300); ?>
                            <div class="readMore">
                                <a href="<?php echo base_url();?>home/detail/<?php echo $p->slug;?>"> Read more</a> 
                            </div>
                            </p>
                        </div>
                        <div class="postMeta">
                            <div class="postType">
                                <div class="iconContainer"> <i class="fa fa-thumb-tack"></i> 
                                </div>
                            </div>
                            <span class="post-comments"><i class="fa fa-folder-o"></i> <a href="">Standard</a></span>  <span class="post-comments"><i class="fa fa-comments-o"></i> <a href="">comments</a></span> 
                        </div>
                    </div>
<?php endforeach; ?>
                <!--endf of post--> 
                <!--  side bar  -->
                <div class="sideBar">
                    <div class="tabWidget">
                        <ul class="tabs">
                            <li>
                                <input type="radio" checked name="tabs" id="tab1">
                                <label for="tab1">Popular Post</label>

                                <div id="tab-content1" class="tab-content">
                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/assets/home/images/right-sidebar-150x150.jpg">
                                            </a>
                                        </div>
                                        <div class="info">
                                            <span class="widgettitle"> 
                                                <a href="http://demo.bloompixel.com/goblog/2014/03/16/blog-post-with-right-sidebar/"
                                                   title="Blog Post with Right Sidebar">Blog Post with Right Sidebar</a>
                                            </span>
                                            <span class="meta">
                                                <div datetime="2014-03-16"><i class="fa fa-clock-o"></i> March 16, 2014</div>
                                            </span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/assets/home/images/venice-150x150.jpg">
                                            </a>
                                        </div>
                                        <div class="info"> <span class="widgettitle"> <a href="http://demo.bloompixel.com/goblog/2014/02/28/blog-post-with-youtube-video/"
                                                                                         title="Blog Post with Youtube Video">Blog Post with Youtube Video</a> </span>  <span class="meta">
                                                <div datetime="2014-02-28"><i class="fa fa-clock-o"></i> February 28, 2014</div>
                                            </span> 
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/assets/home/images/muffins-150x150.jpg">
                                            </a>
                                        </div>
                                        <div class="info"> <span class="widgettitle"> <a href="" title="Blog Post with Tiled Gallery">Blog Post with tiled Gallery</a> </span>  <span class="meta">
                                                <div datetime="2014-02-28"><i class="fa fa-clock-o"></i> February 28, 2014</div>
                                            </span> 
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/assets/home/images/locaked-gate-150x150.jpg">
                                            </a>
                                        </div>
                                        <div class="info"> <span class="widgettitle"> <a href="" title="Standard Blog Post with Featured Image">Standard Blog Post with Featured Image</a> </span>  <span class="meta">
                                                <div datetime="2014-02-28"><i class="fa fa-clock-o"></i> February 28, 2014</div>
                                            </span> 
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </li>


                            <li>
                                <input type="radio" name="tabs" id="tab2">
                                <label for="tab2">Recent Post</label>
                                <div class="clear"></div>

                                <div id="tab-content2" class="tab-content">

                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/assets/home/images/window.jpg">
                                            </a>
                                        </div>
                                        <div class="info">
                                            <span class="widgettitle"> 
                                                <a href="http://demo.bloompixel.com/goblog/2014/03/16/blog-post-with-right-sidebar/"
                                                   title="Blog Post with Right Sidebar">Blog Post with Right Sidebar</a>
                                            </span>
                                            <span class="meta">
                                                <div datetime="2014-03-16"><i class="fa fa-clock-o"></i> March 16, 2014</div>
                                            </span> 
                                        </div>
                                    </div>
                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/assets/home/images/venice-150x150.jpg">
                                            </a>
                                        </div>
                                        <div class="info"> <span class="widgettitle"> <a href="http://demo.bloompixel.com/goblog/2014/02/28/blog-post-with-youtube-video/" title="Blog Post with Youtube Video">Blog Post with Youtube Video</a> </span>  
                                            <span class="meta">
                                                <div datetime="2014-02-28"><i class="fa fa-clock-o"></i> February 28, 2014</div>
                                            </span> 
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/assets/home/images/muffins-150x150.jpg">
                                            </a>
                                        </div>
                                        <div class="info"> <span class="widgettitle"> <a href="" title="Blog Post with Tiled Gallery">Blog Post with tiled Gallery</a> </span>  <span class="meta">
                                                <div datetime="2014-02-28"><i class="fa fa-clock-o"></i> February 28, 2014</div>
                                            </span> 
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="contentinside">
                                        <div class="thumnail">
                                            <a href="">
                                                <img src="<?php echo base_url(); ?>/assets/home/images/locaked-gate-150x150.jpg">
                                            </a>
                                        </div>
                                        <div class="info"> <span class="widgettitle"> <a href="" title="Standard Blog Post with Featured Image">Standard Blog Post with Featured Image</a> </span>  <span class="meta">
                                                <div datetime="2014-02-28"><i class="fa fa-clock-o"></i> February 28, 2014</div>
                                            </span> 
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>

                            </li>
                            <div class="clear"></div>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>                        <!-- closing of sidebar -->
                <div class="clear"></div> 
            </div>                   <!-- closing of container -->
        </div>                       <!-- closing of page -->
    </div>   <!-- closing of mainwrapper -->
    <section id="quote">
        <div class="mainWrapper">
            <div class="page">
                <div class="container">
                    <div class="quoteContent">
                        <div class="quoteSign fa fa-quote-right"></div>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority look even slightly believable.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--   footer section -->

    <!--   footer ends-->

    <script src="<?php echo base_url(); ?>assets/home/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/home/js/slider.js"></script>
    <script src="<?php echo base_url(); ?>assets/home/js/jquery.cycle.all.js"></script>
      <!--  <script>
           $(document).ready(function () {
               $(".panel_button").click(
                   function () {
                       $("#panel").animate({
                           height: "400px"
                       });
                       $("panel_button").toggle();
                   });
               $("#hide_button").click(function () {
                   $("#panel").animate({
                       height: "0px"
                   });
               });
           });
       </script> -->
    <script>
        $("#site-header-wrapper .icon").click(function() {
            $('.nav-menu').slideToggle();
        });

    </script>

</body>
</html>
