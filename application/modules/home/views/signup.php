<body>
    <div id="centerpage-wrapper">

        <div class="page">
            <div class="panel">
                <div class="panel-body">
                    <?php echo validation_errors();?>
                    <form method="POST" action="<?php echo base_url(); ?>user/add" class="form-class" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label" required="true">Name
                                <span class="required">*</span></label>            
                            <input class="input-name" name="full_name" type="text"> 
                            <span class="error">please enter the name</span>           
                        </div>
                        <div class="form-group">
                            <label class="control-label" required="true">Email
                                <span class="required">*</span></label>            
                            <input class="input-name" name="email" type="text"> 
                            <span class="error">please enter the Email</span>           
                        </div>
                        <div class="form-group">
                            <label class="control-label" required="true">Password
                                <span class="required">*</span></label>            
                            <input class="input-name" name="password" type="password"> 
                            <span class="error">please enter the Password</span>           
                        </div>
                        <div class="form-group">
                            <label class="control-label" required="true">Confirm Password
                                <span class="required">*</span></label>            
                            <input class="input-name" name="password_confirm" type="password"> 
                            <span class="error">Confirm Password</span>           
                        </div>

                        <div class="form-group input-radio">

                            <label for="gender" class="control-label" required="1">Gender<span class="required">*</span>
                            </label>            
                            <div class="input-radio-options">
                                <input id="Male_Male" checked="checked" name="gender" type="radio" value="Male">    
                                <label class="control-label" for="Male_Male">Male</label>

                                <input id="Female_Female" name="gender" type="radio" value="Female">    
                                <label class="control-label" for="Female_Female">Female</label>

                                <input id="Other_Other" name="gender" type="radio" value="Other">    
                                <label class="control-label" for="Other_Other">Other</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" required="true">Date of birth
                                <span class="required">*</span></label>            
                            <input class="input-name" name="dob" type="date">    
                        </div>

                        <div class="form-group">
                            <label class="control-label" >Contact no </label>     
                            <input class="input-name" name="phone" type="tel">    
                        </div>

                        <div class="form-group">

                            <label class="control-label">District</label>            
                            <input class="input-name" name="district" type="text">    
                        </div>
                        <div class="form-group">

                            <label class="control-label">VDC/Municipality</label>            
                            <input class="input-name" name="vdc" type="text">    
                        </div>
                        <div class="form-group">

                            <label class="control-label">Ward</label>            
                            <input class="input-name" name="ward" type="number">    
                        </div>
                        <div class="form-group">
                            <label class="control-label">Image</label>            
                            <input class="input-name" name="userfile" type="file">    
                        </div>

                        <div class="form-group">

                            <label class="control-label" required="true">Blood Type<span class="required">*</span></label>            

                            <select class="input-name" id="ethnicity" name="blood_type">
                                <option value="">Select Blood Type</option>
                                <option value="">select any one</option>
                                <option value="A+">A+</option>
                                <option value="A-">A-</option>
                                <option value="B+">B+</option>
                                <option value="B-">B-</option>
                                <option value="AB+">AB+</option>
                                <option value="AB-">AB-</option>
                                <option value="O+">O+</option>
                                <option value="O-">O-</option>
                            </select>        
                        </div>

                        <div class="action-btns">
                            <button href="http://localhost/fightvaw/public/cases/1/edit" class="submit-btn">Sign up</button>
                        </div>
                    </form>
                </div>
            </div>  <!-- closing of page  -->
        </div>

        <!--   footer section -->

        <!-- footer ends -->
</body>
