<?php

class Home_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        echo "here";
        exit;
    }

    function login($username, $password) {
        $this->load->library('session');
        $this->db->where("username", $username);
        $this->db->where("password", $password);

        $query = $this->db->get("user");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                //add all data to session
                $newdata = array(
                    'user_id' => $rows->id,
                    'user_name' => $rows->username,
                    'user_email' => $rows->email,
                    'logged_in' => TRUE,
                );
            }
            $this->session->set_userdata($newdata);
            return true;
        }
        // else {
        //   echo 'error';
        // }
        return false;
    }

    function getsearch() {  

        $blood_type = $this->input->post('blood_type');
        $location = $this->input->post('location');
        $query = $this->db->query("SELECT * FROM donor where blood_type='$blood_type' AND location='$location'");
        return $query->result();
    }

    function add_user() {

        $this->db->where('username', $this->input->post('user_name'));
        $query = $this->db->get('user');

        if ($query->num_rows > 0) {
            //     echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>';
            //   echo "Username already taken";  
            // echo '</strong></div>';
            return false;
        } else {
            // $new_member_insert_data = array(
            //   'first_name' => $this->input->post('first_name'),
            //   'last_name' => $this->input->post('last_name'),
            //   'email_addres' => $this->input->post('email_address'),      
            //   'user_name' => $this->input->post('username'),
            //   'pass_word' => md5($this->input->post('password'))            
            // );
            // $insert = $this->db->insert('membership', $new_member_insert_data);
            //   return $insert;
            $data = array(
                'username' => $this->input->post('user_name'),
                'email' => $this->input->post('email_address'),
                'password' => md5($this->input->post('password'))
            );
            $insert = $this->db->insert('user', $data);
            return $insert;
        }
    }

    public function getSingle($id) {
        $query = $this->db->query("SELECT * FROM user WHERE id='$id'");
        return $query->row($id);
    }

    /* public function add_user()
      {
      $data=array(
      'username'=>$this->input->post('user_name'),
      'email'=>$this->input->post('email_address'),
      'password'=>md5($this->input->post('password'))
      );
      $this->db->insert('user',$data);
      } */
}
?>

