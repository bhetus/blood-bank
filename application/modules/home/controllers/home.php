<?php

class Home extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('slider/slider_model');
        $this->load->model('page/page_model');
    }

    public function index() {
        if ($this->session->userdata('is_logged_in')) {
            /* $data['main_content'] = 'includes/search_view.php';
              $this->load->view('includes/template', $data); */
            /* $this->load->view('includes/signed_header');
              $this->load->view('includes/welcome_view');
              $this->load->view('includes/footer');
              if(($this->session->userdata('user_name')!=""))
              { */
            $this->welcome();
        } else {
            // $data['title']= 'Home';
            $this->load->view('home/main_header');
            $this->load->view('home/site/index');
            $this->load->view('home/footer');
        }
    }

    public function welcome() {
//  print_r($this->session->userdata);
        // $data['title']= 'Welcome';
        $this->load->view('signed_header');
        $this->load->view('site/index');
        $this->load->view('footer');
    }

    // Check for user login process
    public function login() {
        /* $this->load->library('form_validation');

          $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|');
          $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

          if ($this->form_validation->run() == FALSE) {
          $this->load->view('error_signin');
          $this->load->view('site/index');
          $this->load->view('footer');

          } else { */

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->user_model->login($username, $password);
        if ($result == TRUE) {
            $uid = $this->session->userdata('user_id');
            $user = $this->user_model->getSingle($uid);
            $type = $user->type;
            if ($type == "user") {
                redirect('user/welcome');
            } else {
                echo "i am admin";
            }
            /* $sess_array = array(
              'username' => $this->input->post('username'),
              'password' => $this->input->post('password')
              ); */
            /* $this->load->view('signed_header');
              $this->load->view('site/index');
              $this->load->view('footer'); */
            // redirect ('user/welcome');
        } else {
            var_dump('error');
            $this->load->view('error_signin');
            var_dump('error');
            $this->load->view('site/index');
            $this->load->view('footer');
            // redirect ('user/index');
        }
    }

    public function search() {
        $data['result'] = $this->home_model->getsearch();
        $this->load->view('home/main_header');
        $this->load->view('home/site/index', $data);
        $this->load->view('home/footer');
    }

    public function logout() {
        $newdata = array(
            'user_id' => '',
            'user_name' => '',
            'user_email' => '',
            'logged_in' => FALSE,
        );
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        redirect('user/index');
    }

    public function blog() {
        $data['slider'] = $this->slider_model->getPublish();
        $data['posts'] = $this->page_model->getAllonHome();
        $this->load->view('home/main_header');
        $this->load->view('home/blog', $data);
        $this->load->view('home/footer');
    }

    public function detail($slug) {
        $data['post'] = $this->page_model->getSingleFront($slug);
        $data['recent'] = $this->page_model->getAllonHome();
        $this->load->view('home/main_header');
        $this->load->view('home/blog_detail', $data);
        $this->load->view('home/footer');
    }

    public function register() {
        $this->load->view('home/main_header');
        $this->load->view('home/signup');
        $this->load->view('home/footer');
    }

}

?>