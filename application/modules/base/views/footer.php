
<!-- Footer -->
<footer class="main">


    &copy; 2014 <strong>The Last Resort</strong> Reservation System By <a href="http://smartsamaj.com" target="_blank">SmartSamaj</a>

</footer>
</div>

</div>


<!-- Bottom Scripts -->
<script src="<?php echo base_url(); ?>assets/js/gsap/main-gsap.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/js/joinable.js"></script>
<script src="<?php echo base_url(); ?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-api.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-login.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-demo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/neon-forgotpassword.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.inputmask.bundle.min.js"></script>

</body>
</html>