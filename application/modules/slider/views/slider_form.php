<ol class="breadcrumb bc-3">
    <li>
        <a href="index.html"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url(); ?>slider">Slider</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>slider" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Slider
</a>
<h2>Add Slider</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">
	
	<div class="col-md-6">
	<div class="form-group">
                <label class="control-label">Slider Image</label><br>

                <input type="file" class="form-control file2 inline btn btn-primary" multiple="1" data-label="<i class='glyphicon glyphicon-circle-arrow-up'></i> &nbsp;Browse Files" name="userfile" />
            </div>
            </div>


	   	   <div class="col-md-11">
 <div class="form-group">
                <label class="control-label">Slider Description</label>
	<textarea class="form-control ckeditor" name="description" data-validate="required">
<?php if (!empty($single->description)) {
    echo $single->description;
} else {
    echo set_value('description');
}
?>
                </textarea>
</div>
</div>

	    <div class="col-md-11">
            <div class="form-group">
                <label class="control-label">Link</label>

                <input type="text" class="form-control" name="link" data-validate="required" data-message-required="This field is required" placeholder="Link here" value="<?php if(!empty($single)){ echo $single->link;}?>"/>
            </div>
	    </div>

	    <div class="col-md-11">
            <div class="form-group">
                <label class="control-label">Publish</label>
                <ul class="icheck-list">
                    <li>
                        <input tabindex="5" type="checkbox" class="icheck" name="publish" value="1" id="minimal-checkbox-1" <?php if(!empty($single)){ if($single->publish=="1"){echo "checked";}}?>>
                        <label for="minimal-checkbox-1"></label>
                    </li>
                  
                </ul>
	    </div>
	    </div>

	<div class="col-md-6">
      <div class="form-group">
                <button type="submit" class="btn btn-success" name="submit">Submit</button>
                <button type="reset" class="btn">Reset</button>
            </div>
        </form>

    </div>
	</div>

</div><!-- Footer -->
