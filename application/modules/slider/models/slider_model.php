<?php

class Slider_model extends CI_Model {
    
    private $table = "mk_slider";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($img='',$id='') {
      
        $data = array(
	    'image'=>$img,
            'description' => $this->input->post('description'),
            'link' => $this->input->post('link'),      
	    'publish' => $this->input->post('publish') 
        );

        if ($id == '') {
		
            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {           
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message',"Information updated successfully");
        }
    }

   

    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

  
    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }
    
    function delete($id){
        $this->db->delete($this->table,array('id'=>$id));
        $this->session->set_flashdata('message',"Information Deleted successfully");        
    }

    function getPublish(){
	$data=$this->db->query("SELECT * FROM $this->table WHERE publish='1'");
	return $data->result();
    }

   
}

?>
