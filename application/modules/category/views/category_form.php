<ol class="breadcrumb bc-3">
    <li>
        <a href="index.html"><i class="entypo-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url(); ?>category">Category</a>
    </li>
    <li class="active">

        <strong>Add</strong>
    </li>
</ol>
<a href="<?php echo base_url(); ?>category" class="btn btn-blue">
    <i class="entypo-search"></i>
    View Category
</a>
<h2>Add Category</h2>
<br />

<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="panel-title">All fields have validation rules <small></small></div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <div class="panel-body">

        <form role="form" id="form1" method="post" class="validate" action="" enctype="multipart/form-data">

	    <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Category Name</label>

                <input type="text" class="form-control" name="category_name" data-validate="required" data-message-required="This field is required" placeholder="Category name here" value="<?php if(!empty($single)){ echo $single->cat_name;}?>"/>
            </div>
	    </div> 
	          
			<div class="col-md-6">
	                <div class="form-group">
                <label class="control-label">Parent Of : </label>

                <select class="form-control" name="parent_id"> 
			<option value="0">none</option>
			<?foreach($parent as $p): ?>
			<option value="<?php echo $p->id;?>" <?php if(!empty($single)) { if($p->id==$single->parent_id){echo 'selected="selected"';}}?>><?php echo $p->cat_name;?></option>
			<?php endforeach; ?>
                </select>
            </div>
	    </div>
	
	<div class="col-md-11">
	<div class="form-group">
                <label class="control-label">Images</label><br>

                <input type="file" class="form-control file2 inline btn btn-primary" multiple="1" data-label="<i class='glyphicon glyphicon-circle-arrow-up'></i> &nbsp;Browse Files" name="userfile" />
            </div>
            </div>


	   <div class="col-md-11">
 <div class="form-group">
                <label class="control-label">Description</label>
	<textarea class="form-control ckeditor" name="description" data-validate="required">
<?php if (!empty($single->description)) {
    echo $single->description;
} else {
    echo set_value('description');
}
?>
                </textarea>
</div>
</div>
	 
	    <div class="col-md-6">
	    <div class="form-group">
                <label class="control-label">Meta Title</label>
 		<textarea class="form-control autogrow" name="meta_title" data-validate="minlength[10]" rows="5" placeholder="Meta Title here" ><?php if(!empty($single)){ echo $single->meta_title;}?></textarea>
            </div>
	    </div>
	  
            <div class="col-md-6">
	    <div class="form-group">
                <label class="control-label">Meta Keyword</label>
		<textarea class="form-control autogrow" name="meta_keyword" data-validate="minlength[10]" rows="5" placeholder="Meta Title here"><?php if(!empty($single)){ echo $single->meta_keyword;}?></textarea>
            </div>
	    </div>
	
	    <div class="col-md-6">
	    <div class="form-group">
                <label class="control-label">Meta Description</label>
		<textarea class="form-control autogrow" name="meta_description" data-validate="minlength[10]" rows="5" placeholder="Meta Description here"><?php if(!empty($single)){ echo $single->meta_description;}?></textarea>
            </div>
	    </div>

	    <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Publish</label>
                <ul class="icheck-list">
                    <li>
                        <input tabindex="5" type="checkbox" class="icheck" name="publish" value="1" id="minimal-checkbox-1" <?php if(!empty($single)){ if($single->publish=="1"){echo "checked";}}?>>
                        <label for="minimal-checkbox-1"></label>
                    </li>
                  
                </ul>
	    </div>
	    </div>

	   <div class="col-md-6">
	   <div class="form-group">
                <label class="control-label">Featured</label>
                <ul class="icheck-list">
                    <li>
                        <input tabindex="5" type="checkbox" class="icheck" name="featured" value="1" id="minimal-checkbox-1" <?php if(!empty($single)){ if($single->featured=="1"){echo "checked";}}?>>
                        <label for="minimal-checkbox-1"></label>
                    </li>
                  
                </ul>
	    </div>
	    </div>

	<div class="col-md-11">
      <div class="form-group">
                <button type="submit" class="btn btn-success" name="submit">Submit</button>
                <button type="reset" class="btn">Reset</button>
            </div>
        </form>

    </div>
	</div>

</div><!-- Footer -->
