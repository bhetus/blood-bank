<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <title><?php if (!empty($title)) {
    echo $title;
} else {
    echo "Travel management system";
} ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Travel Website CMS by makura creations">
        <meta name="author" content="bhetus" >
        <!--------ligthbox jquery--------------->
        <script src="<?php echo base_url(); ?>/admin/js/jquery-1.10.2.min.js"></script>
        <script src="<?php echo base_url(); ?>/admin/js/lightbox-2.6.min.js"></script>
        <link href="<?php echo base_url(); ?>/admin/css/lightbox.css" rel="stylesheet" />
      
        <!-- for tiny mce editor -->
<?php $url = $this->uri->segment(1);
if ($url != "image_controller") {
    ?>
            <script type="text/javascript" src="<?php echo base_url(); ?>admin/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

            <script type="text/javascript">
                tinyMCE.init({
                    // General options
                    mode : "specific_textareas",
                    theme : "advanced",
                    editor_selector: "editor",
                    entity_encoding : "raw",
                    plugins : "openmanager,autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

                    // Theme options
                    theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
                    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                    theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "left",
                    theme_advanced_statusbar_location : "bottom",
                    theme_advanced_resizing : true,
//open manager plugins
                    file_browser_callback: "openmanager",
                    open_manager_upload_path: '../../../../../../uploadfolder/',
                    open_manager_upload_path: '../../../../../../uploadfolder/',
                    // Skin options
                    skin : "o2k7",
                    skin_variant : "silver",

                    // Example content CSS (should be your site CSS)
                    content_css : "css/example.css",

                    // Drop lists for link/image/media/template dialogs
                    template_external_list_url : "js/template_list.js",
                    external_link_list_url : "js/link_list.js",
                    external_image_list_url : "js/image_list.js",
                    media_external_list_url : "js/media_list.js",

                    // Replace values for the template plugin
                    template_replace_values : {
                        username : "Some User",
                        staffid : "991234"
                    }
                });
            </script>
<?php } ?>
        <!-- tiny mce editor ends here -->
        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>admin/js/jquery-1.7.2.min.js"></script>
        <!-- jQuery UI -->
               <!-- custom dropdown library -->
        <script src="<?php echo base_url(); ?>admin/js/bootstrap-dropdown.js"></script>
        <!-- The styles -->
        <link id="bs-css" href="<?php echo base_url() ?>/admin/css/bootstrap-cerulean.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
                <!--background-color: #555;-->
            }
            .sidebar-nav {
                padding: 9px 0;
            }
            .new {	background-color:black; color:white; padding:5px; border-radius:5px;} 
            .new:hover{border:1px dotted black;color: red;}
        </style>
        <link href="<?php echo base_url(); ?>admin/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>admin/css/charisma-app.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>admin/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo base_url() ?>/admin/img/favicon.ico">

    </head>

    <body>

            <!-- topbar starts -->
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container-fluid">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <h2 style="float:left;padding-right: 8px;">Travel Management System</h1>

                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right" >
                            <a class="btn dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-user"></i><span class="hidden-phone">
    <?php echo $this->session->userdata('username'); ?></span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url();?>users/auth/change_password/">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>index.php/users/auth/logout" onclick="javascript:return confirm('Are you sure you want to logout??')">Logout</a></li>
                            </ul>
                        </div>
                        <!-- user dropdown ends -->

                        <div class="top-nav nav-collapse">
                            <ul class="nav">
                                <li><a href="<?php echo base_url(); ?>" target="_blank">Visit Site</a></li>
                               
<!--                                <li>
                                    <form class="navbar-search pull-left">
                                        <input placeholder="Search" class="search-query span2" name="query" type="text">
                                    </form>
                                </li>-->
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- topbar ends -->

        <div class="container-fluid">
            <div class="row-fluid">


                    <!-- left menu starts -->
                    <div class="span2 main-menu-span">
                        <div class="well nav-collapse sidebar-nav">
                            <ul class="nav nav-tabs nav-stacked main-menu">
                                <li class="nav-header hidden-tablet">Main</li>
                                <li><a class="ajax-link" href="<?php echo site_url();?>users/auth"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/category/category"><i class="icon-list-alt"></i><span class="hidden-tablet">Category</span></a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/page/page"><i class="icon-list-alt"></i><span class="hidden-tablet">Page</span></a></li>
                                <li><a class="ajax-link" href="<?php echo base_url(); ?>index.php/destination/destination"><i class="icon-picture"></i><span class="hidden-tablet"> Destination</span></a></li>
                                <li><a class="ajax-link" href="<?php echo base_url(); ?>index.php/activity/activity"><i class="icon-edit"></i><span class="hidden-tablet"> Activity</span></a></li>

                                <li><a class="ajax-link" href="<?php echo base_url(); ?>index.php/region/region"><i class="icon-edit"></i><span class="hidden-tablet"> Region</span></a></li>
                                <li><a class="ajax-link" href="<?php echo base_url(); ?>index.php/trip/trip"><i class="icon-list-alt"></i><span class="hidden-tablet"> Trip</span></a></li>
                                <li><a class="ajax-link" href="<?php echo base_url(); ?>index.php/itinerary/itinerary"><i class="icon-list-alt"></i><span class="hidden-tablet"> Itinerary</span></a></li>
                                <li><a class="ajax-link" href="<?php echo base_url(); ?>index.php/fixed_departure/fixed_departure"><i class="icon-list-alt"></i><span class="hidden-tablet"> Fixed Departure</span></a></li>
                                <li><a class="ajax-link" href="<?php echo base_url(); ?>index.php/slider/slider"><i class="icon-font"></i><span class="hidden-tablet"> Slider</span></a></li>
                                <li><a class="ajax-link" href="<?php echo base_url(); ?>index.php/album/album"><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>

                                <li><a href="<?php echo base_url(); ?>index.php/testimonial/testimonial/testimonial"><i class="icon-user"></i><span class="hidden-tablet">Testimonials</span></a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/contact/contact"><i class="icon-user"></i><span class="hidden-tablet">Contacts</span></a></li>

                        </div><!--/.well -->
                    </div><!--/span-->
                    <!-- left menu ends -->

                   

                    <div id="content" class="span10">
                        <!-- content starts -->

