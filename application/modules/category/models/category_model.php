<?php

class Category_model extends CI_Model {
    
    private $table = "mk_category";

    function construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('admin/post');
    }

    /*
     * Function to add destination from form
     */

    function manage($img='',$id='') {
	$name=$this->input->post('category_name');
	$slug = url_title($name, 'dash', true);
      
        $data = array(
	    'parent_id' => $this->input->post('parent_id'),
            'cat_name' => $this->input->post('category_name'),
            'slug' => $slug,
	    'image'=>$img,
            'description' => $this->input->post('description'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_keyword' => $this->input->post('meta_keyword'),    
	    'meta_description' => $this->input->post('meta_description'),       
	    'publish' => $this->input->post('publish'),
	    'featured' => $this->input->post('featured'),   
        );

        if ($id == '') {
            $this->db->insert($this->table, $data);
            $this->session->set_flashdata('message', "Insertion successfull");
        } else {           
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            $this->session->set_flashdata('message',"Information updated successfully");
        }
    }

   
    function getAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

  
    function getSingle($id) {
        $data = $this->db->query("SELECT * FROM $this->table WHERE id='$id'");
        return $data->row($id);
    }
    
    function getParent(){
	$data = $this->db->query("SELECT * FROM $this->table WHERE parent_id=0");
	return $data->result();
    }

    function getChild(){
	$data = $this->db->query("SELECT * FROM $this->table WHERE parent_id!=0");
	return $data->result();
    }
    
    function delete($id){
        $this->db->delete($this->table,array('id'=>$id));
        $this->session->set_flashdata('message',"Information Deleted successfully");        
    }

	
    function category(){
	$sql = $this->db->query("select * from $this->table where parent_id='0'");
	$output = "";
	$result = $sql->result();
	$output .= "<ul class='dropdown-menu multi-level' role='menu' aria-labelledby='dropdownMenu' >";
	foreach($result as $ra){
	$output .= "<li class='dropdown-submenu'><a href='".base_url().$ra->slug."'>".$ra->cat_name."</a>";
	$output.=$this->get_child($ra->id);
	$output .= "</li>";
		}
		$output .= "</ul>";
		return $output;
     }
	
     function get_child($parent_id){
	$sql  = $this->db->query("select * from $this->table where parent_id=".$parent_id);
	$r = $sql->result();
	$output1 = "";
	if(!empty($r))
	{
		$output1.= "<ul class='dropdown-menu'>";
		foreach($r as $s){
		$output1 .= "<li><a href='".base_url()."home/product/".$s->slug."'>".$s->cat_name."</a>";
		$this->get_child($s->id);
			}
			$output1.= "</ul>";
			}
			return $output1;
			}

 function getSlugCategory($slug){
	$data=$this->db->query("SELECT * FROM $this->table WHERE slug='$slug'");
	return $data->row($slug);
    }


    function getFeaturedCategory(){
	$data=$this->db->query("SELECT * FROM $this->table WHERE featured='1' AND parent_id!='0'");
	return  $data->result();
	
	//return $res;
    }

   
    function countProducts($id){
	$data = $this->db->query("SELECT count(id) as count from mk_product where cat_id=$id");
	$res = $data->row($id);	
	return $res->count;
    }
   
}
?>
